// test_couleur.cpp : Seuille une image en niveau de gris

#include <iostream>
#include <stdio.h>
#include "image_ppm.h"
#include <cmath>

OCTET nuance_moyenne_voisin(int x, int y, OCTET* ImgIn,  OCTET* Masque, int R, int nW);

int main(int argc, char* argv[])
{
  char cNomImgLue1[250], cNomImgLue2[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue1) ;
   sscanf (argv[2],"%s",cNomImgLue2);

   OCTET *ImgIn1, *ImgIn2;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue1, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn1, OCTET, nTaille);
   lire_image_pgm(cNomImgLue1, ImgIn1, nH * nW);
   allocation_tableau(ImgIn2, OCTET, nTaille);
   lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);
   int cmp = 0;
   int eqm;
   for (int i=0; i < nW; i++){
     for (int j=0; j < nW; j++){
       cmp += (ImgIn1[i*nW+j] - ImgIn2[i*nW+j]) * (ImgIn1[i*nW+j] - ImgIn2[i*nW+j]);
     }
   }
  
   eqm = cmp/nTaille;

   printf("%d \n", eqm);
   free(ImgIn1); free(ImgIn2);

   return 1;
}

